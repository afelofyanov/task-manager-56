package ru.tsc.felofyanov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.tsc.felofyanov.tm.dto.request.ServerVersionRequest;
import ru.tsc.felofyanov.tm.dto.response.ServerVersionResponse;

@Component
public final class VersionCommand extends AbstractSystemCommand {

    @NotNull
    @Override
    public String getName() {
        return "version";
    }

    @NotNull
    @Override
    public String getArgument() {
        return "-v";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show applicant version.";
    }

    @Override
    public void execute() {
        System.out.println("[VERSION]");
        System.out.println("Client: " + getPropertyService().getApplicationVersion());
        @NotNull final ServerVersionResponse response = getSystemEndpoint().getVersion(new ServerVersionRequest());
        System.out.println("Client: " + response.getVersion());
    }
}
