package ru.tsc.felofyanov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.tsc.felofyanov.tm.dto.request.ProjectChangeStatusByIndexRequest;
import ru.tsc.felofyanov.tm.enumerated.Status;
import ru.tsc.felofyanov.tm.util.TerminalUtil;

import java.util.Arrays;

@Component
public final class ProjectChangeStatusByIndexCommand extends AbstractProjectCommand {

    @NotNull
    @Override
    public String getName() {
        return "project-change-status-by-index";
    }

    @Override
    @Nullable
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Change project status by index.";
    }

    @Override
    public void execute() {
        System.out.println("[CHANGE PROJECT STATUS BY INDEX]");

        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;

        System.out.println("ENTER STATUS:");
        System.out.println(Arrays.toString(Status.values()));
        @NotNull final String statusValue = TerminalUtil.nextLine();
        @Nullable final Status status = Status.toStatus(statusValue);

        @NotNull ProjectChangeStatusByIndexRequest request =
                new ProjectChangeStatusByIndexRequest(getToken(), index, status);
        getProjectEndpoint().changeProjectStatusByIndex(request);
    }
}
