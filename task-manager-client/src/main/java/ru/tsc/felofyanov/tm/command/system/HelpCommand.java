package ru.tsc.felofyanov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.tsc.felofyanov.tm.command.AbstractCommand;

import java.util.Collection;

@Component
public final class HelpCommand extends AbstractSystemCommand {

    @NotNull
    @Override
    public String getName() {
        return "help";
    }

    @NotNull
    @Override
    public String getArgument() {
        return "-h";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show terminal commands info.";
    }

    @Override
    public void execute() {
        System.out.println("[Help]");
        @NotNull final Collection<AbstractCommand> commands = getCommandService().getCommands();
        for (final AbstractCommand command : commands)
            System.out.println(command);
    }
}
