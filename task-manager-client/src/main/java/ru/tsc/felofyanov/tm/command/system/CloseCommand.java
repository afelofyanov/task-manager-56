package ru.tsc.felofyanov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;

@Component
public final class CloseCommand extends AbstractSystemCommand {

    @NotNull
    @Override
    public String getName() {
        return "exit";
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Close application.";
    }

    @Override
    public void execute() {
        System.out.println("Completing the task-manager...");
        System.exit(0);
    }
}
