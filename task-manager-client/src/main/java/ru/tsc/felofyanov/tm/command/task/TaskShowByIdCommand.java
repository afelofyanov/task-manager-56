package ru.tsc.felofyanov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.tsc.felofyanov.tm.dto.model.TaskDTO;
import ru.tsc.felofyanov.tm.dto.request.TaskGetByIdRequest;
import ru.tsc.felofyanov.tm.dto.response.TaskGetByIdResponse;
import ru.tsc.felofyanov.tm.util.TerminalUtil;

@Component
public final class TaskShowByIdCommand extends AbstractTaskCommand {

    @NotNull
    @Override
    public String getName() {
        return "task-show-by-id";
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show task by id.";
    }

    @Override
    public void execute() {
        System.out.println("[SHOW TASK BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();

        @NotNull final TaskGetByIdRequest request = new TaskGetByIdRequest(getToken(), id);
        @NotNull final TaskGetByIdResponse response = getTaskEndpoint().getTaskById(request);
        @Nullable final TaskDTO task = response.getTask();
        showTask(task);
    }
}
