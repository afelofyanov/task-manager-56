package ru.tsc.felofyanov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.tsc.felofyanov.tm.dto.request.DataSaveJsonFasterXmlRequest;
import ru.tsc.felofyanov.tm.enumerated.Role;

@Component
public final class DataSaveJsonFasterXmlCommand extends AbstractDataCommand {

    @NotNull
    @Override
    public String getName() {
        return "data-save-json-fasterxml";
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Save data in json file(FasterXML)";
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    @SneakyThrows
    public void execute() {
        getDomainEndpoint().saveDataJsonFasterXml(new DataSaveJsonFasterXmlRequest(getToken()));
    }
}
