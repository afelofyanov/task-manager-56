package ru.tsc.felofyanov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.tsc.felofyanov.tm.dto.request.ProjectCreateRequest;
import ru.tsc.felofyanov.tm.util.TerminalUtil;

import java.util.Date;

@Component
public final class ProjectCreateCommand extends AbstractProjectCommand {

    @NotNull
    @Override
    public String getName() {
        return "project-create";
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Create new project.";
    }

    @Override
    public void execute() {
        System.out.println("[PROJECT CREATE]");

        System.out.println("ENTER NAME: ");
        @NotNull final String name = TerminalUtil.nextLine();

        System.out.println("ENTER DESCRIPTION: ");
        @NotNull final String description = TerminalUtil.nextLine();

        System.out.println("ENTER DATE BEGIN: ");
        @Nullable final Date dateBegin = TerminalUtil.nextDate();

        System.out.println("ENTER DATE END: ");
        @Nullable final Date dateEnd = TerminalUtil.nextDate();

        @NotNull final ProjectCreateRequest request =
                new ProjectCreateRequest(getToken(), name, description, dateBegin, dateEnd);
        getProjectEndpoint().createProject(request);
    }
}
