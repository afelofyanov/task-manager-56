package ru.tsc.felofyanov.tm.command.taskproject;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tsc.felofyanov.tm.api.endpoint.IProjectTaskEndpoint;
import ru.tsc.felofyanov.tm.command.AbstractCommand;

@Getter
@Component
public abstract class AbstractTaskProjectCommand extends AbstractCommand {

    @NotNull
    @Autowired
    private IProjectTaskEndpoint projectTaskEndpoint;
}
