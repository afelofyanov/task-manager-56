package ru.tsc.felofyanov.tm.api.model;

import org.jetbrains.annotations.Nullable;
import ru.tsc.felofyanov.tm.enumerated.Status;

public interface IHasStatus {

    @Nullable
    Status getStatus();

    void setStatus(@Nullable Status status);
}
