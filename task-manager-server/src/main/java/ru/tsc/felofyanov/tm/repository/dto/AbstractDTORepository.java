package ru.tsc.felofyanov.tm.repository.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.tsc.felofyanov.tm.api.repository.dto.IDTORepository;
import ru.tsc.felofyanov.tm.dto.model.AbstractModelDTO;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Getter
@Repository
@Scope("prototype")
@NoArgsConstructor
@AllArgsConstructor
public abstract class AbstractDTORepository<M extends AbstractModelDTO> implements IDTORepository<M> {

    @NotNull
    @Autowired
    protected EntityManager entityManager;

    @NotNull
    protected Class<M> clazz;

    @NotNull
    protected String getModelName() {
        return clazz.getSimpleName();
    }

    @Override
    public M add(@Nullable M model) {
        entityManager.persist(model);
        return model;
    }

    @NotNull
    @Override
    public Collection<M> add(@Nullable Collection<M> collection) {
        final List<M> result = new ArrayList<>();
        collection
                .stream()
                .forEach(model -> result.add(add(model)));
        return result;
    }

    @NotNull
    @Override
    public List<M> findAll() {
        return entityManager.createQuery("FROM " + getModelName(), clazz).getResultList();
    }

    @Override
    public void clear() {
        for (@NotNull final M model : findAll()) entityManager.remove(model);
    }

    @Override
    public boolean existsById(@Nullable String id) {
        return findOneById(id) != null;
    }

    @NotNull
    @Override
    public Collection<M> set(@NotNull Collection<M> models) {
        clear();
        return add(models);
    }

    @Nullable
    @Override
    public M findOneById(@Nullable String id) {
        return entityManager.find(clazz, id);
    }

    @Override
    public M findOneByIndex(@Nullable Integer index) {
        TypedQuery<M> query = entityManager.createQuery("FROM " + getModelName(), clazz).setFirstResult(index);
        @NotNull final M result = query.setMaxResults(1).getResultList().get(0);
        return result;
    }

    @Override
    public M remove(@NotNull M model) {
        entityManager.remove(model);
        return model;
    }

    @Override
    public void removeAll(@Nullable Collection<M> collection) {
        if (collection == null) return;
        collection
                .stream()
                .forEach(this::remove);
    }

    @Override
    public M removeById(@Nullable String id) {
        Optional<M> model = Optional.ofNullable(findOneById(id));
        model.ifPresent(this::remove);
        return model.orElse(null);
    }

    @Override
    public M removeByIndex(@Nullable Integer index) {
        Optional<M> model = Optional.ofNullable(findOneByIndex(index));
        model.ifPresent(this::remove);
        return model.orElse(null);
    }

    @Override
    public M update(@NotNull M model) {
        return entityManager.merge(model);
    }

    @Override
    public long count() {
        return entityManager.createQuery("SELECT COUNT(1) FROM " + getModelName(), Long.class).getSingleResult();
    }
}
