package ru.tsc.felofyanov.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import ru.tsc.felofyanov.tm.api.repository.dto.IDTORepository;
import ru.tsc.felofyanov.tm.api.service.dto.IServiceDTO;
import ru.tsc.felofyanov.tm.dto.model.AbstractModelDTO;
import ru.tsc.felofyanov.tm.exception.entity.ModelNotFoundException;
import ru.tsc.felofyanov.tm.exception.field.IdEmptyException;
import ru.tsc.felofyanov.tm.exception.field.IndexIncorrectException;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.List;

@Service
public abstract class AbstractDTOService<M extends AbstractModelDTO, R extends IDTORepository<M>> implements IServiceDTO<M> {

    @NotNull
    @Autowired
    protected ApplicationContext context;

    @NotNull
    protected abstract IDTORepository<M> getRepository();

    @Override
    public M add(@Nullable final M model) {
        if (model == null) return null;
        @NotNull final IDTORepository<M> repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        @Nullable final M result;
        try {
            entityManager.getTransaction().begin();
            result = repository.add(model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return result;
    }

    @Override
    public Collection<M> add(@Nullable Collection<M> models) {
        @NotNull final IDTORepository<M> repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        @Nullable final Collection<M> result;
        try {
            entityManager.getTransaction().begin();
            result = repository.add(models);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return result;
    }

    @Override
    public M update(@Nullable M model) {
        if (model == null) return null;
        @NotNull final IDTORepository<M> repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        @Nullable final M result;
        try {
            entityManager.getTransaction().begin();
            result = repository.update(model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return result;
    }

    @NotNull
    @Override
    public List<M> findAll() {
        @NotNull final IDTORepository<M> repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.findAll();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public boolean existsById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return findOneById(id) != null;
    }

    @NotNull
    @Override
    public Collection<M> set(@NotNull Collection<M> models) {
        if (models.isEmpty()) return models;
        @NotNull final IDTORepository<M> repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.clear();
            repository.add(models);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return models;
    }

    @Override
    public void clear() {
        @NotNull final IDTORepository<M> repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            repository.clear();
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public M findOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) return null;
        @NotNull final IDTORepository<M> repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.findOneById(id);
        } finally {
            entityManager.close();
        }
    }

    @Override
    @Nullable
    public M findOneByIndex(@Nullable final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        @NotNull final IDTORepository<M> repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.findOneByIndex(index);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public M remove(@Nullable final M model) {
        if (model == null) throw new ModelNotFoundException();
        @NotNull final IDTORepository<M> repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        @Nullable final M result;
        try {
            entityManager.getTransaction().begin();
            result = repository.remove(model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return result;
    }

    @Override
    public M removeById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();

        @Nullable final M result;
        @NotNull final IDTORepository<M> repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();

        try {
            entityManager.getTransaction().begin();
            result = repository.removeById(id);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return result;
    }

    @Override
    public M removeByIndex(@Nullable final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();

        @Nullable final M result;
        @NotNull final IDTORepository<M> repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();

        try {
            entityManager.getTransaction().begin();
            result = repository.removeByIndex(index);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return result;
    }

    @Override
    public void removeAll(@Nullable final Collection<M> collection) {
        if (collection == null || collection.isEmpty()) return;
        @NotNull final IDTORepository<M> repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            repository.removeAll(collection);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public long count() {
        @NotNull final IDTORepository<M> repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.count();
        } finally {
            entityManager.close();
        }
    }
}
