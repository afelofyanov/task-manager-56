package ru.tsc.felofyanov.tm.service;

import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.tsc.felofyanov.tm.api.repository.IDomainRepository;
import ru.tsc.felofyanov.tm.api.service.IDomainService;

@Service
@AllArgsConstructor
public class DomainService implements IDomainService {

    @NotNull
    @Autowired
    protected IDomainRepository domainRepository;

    @Override
    public void loadDataBackup() {
        domainRepository.loadDataBackup();
    }

    @Override
    public void saveDataBackup() {
        domainRepository.saveDataBackup();
    }

    @Override
    public void loadDataBase64() {
        domainRepository.loadDataBase64();
    }

    @Override
    public void saveDataBase64() {
        domainRepository.saveDataBase64();
    }

    @Override
    public void loadDataBinary() {
        domainRepository.loadDataBinary();
    }

    @Override
    public void saveDataBinary() {
        domainRepository.saveDataBinary();
    }

    @Override
    public void loadDataJsonFasterXml() {
        domainRepository.loadDataJsonFasterXml();
    }

    @Override
    public void saveDataJsonFasterXml() {
        domainRepository.saveDataJsonFasterXml();
    }

    @Override
    public void loadDataJsonJaxB() {
        domainRepository.loadDataJsonJaxB();
    }

    @Override
    public void saveDataJsonJaxB() {
        domainRepository.saveDataJsonJaxB();
    }

    @Override
    public void loadDataXmlFasterXml() {
        domainRepository.loadDataXmlFasterXml();
    }

    @Override
    public void saveDataXmlFasterXml() {
        domainRepository.saveDataXmlFasterXml();
    }

    @Override
    public void loadDataXmlJaxB() {
        domainRepository.loadDataXmlJaxB();
    }

    @Override
    public void saveDataXmlJaxB() {
        domainRepository.saveDataXmlJaxB();
    }

    @Override
    public void loadDataYamlFasterXml() {
        domainRepository.loadDataYamlFasterXml();
    }

    @Override
    public void saveDataYamlFasterXml() {
        domainRepository.loadDataYamlFasterXml();
    }
}
