package ru.tsc.felofyanov.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.tsc.felofyanov.tm.api.service.model.IUserService;
import ru.tsc.felofyanov.tm.configuration.ServerConfiguration;
import ru.tsc.felofyanov.tm.model.User;

public abstract class AbstractTest {

    @NotNull
    protected static AnnotationConfigApplicationContext CONTEXT;

    @NotNull
    protected static IUserService userService;

    @NotNull
    protected User testUser;

    @BeforeClass
    public static void initConnectionService() {
        CONTEXT = new AnnotationConfigApplicationContext(ServerConfiguration.class);
    }

    @Before
    public void init() {
        userService = CONTEXT.getBean(IUserService.class);
        testUser = userService.create("testUser", "testUser");
    }

    @After
    public void close() {
        userService.removeByLogin("testUser");
    }
}
